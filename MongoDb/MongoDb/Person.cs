﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace MongoDb
{
    class Person
    {
		public string Firstname;
		public string Middlename;
		public string Lastname;
		public List<string> Alias = new List<string>();
		public DateTime DateOfBirth;
		public DateTime DateOfDeath;
		public string PlaceOfBirth;
		public List<Movie> Movie = new List<Movie>();
		public List<string> Role = new List<string>();
		public string Biography;
    }
	
	public class Movie
	{
		public string title;
		public string posterUrl;
	}
}
