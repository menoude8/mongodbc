﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDb
{
    public partial class Form1 : Form
    {
        const string ConnectionString = "mongodb://localhost";
        private readonly MongoCollection<Film> _collection; 
        private List<Film> _films = new List<Film>(); 

        public Form1()
        {
            InitializeComponent();
            var client = new MongoClient(ConnectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("films");
            _collection = database.GetCollection<Film>("films");
            FillFilms();
        }

        public void FillFilms()
        {
            _films = _collection.FindAll().ToList();
            foreach (var film in _films)
            {
                ls_film.Items.Add(new ListViewItem(new string[] { film.Title, film.Genre.DefaultIfEmpty(string.Empty).Aggregate((s, s1) => s + ", " + s1) }));
            }
        }

        private void ls_film_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ls_film.SelectedIndices.Count == 0)
            {
                txt_title.Text = "";
                ls_actor.Clear();
            }
            try
            {
                var current = _films[ls_film.SelectedIndices[0]];
                txt_title.Text = current.Title;
                ls_actor.Clear();
                foreach (var actor in current.Actor)
                {
                    ls_actor.Items.Add(new ListViewItem(new string[] { actor.name, actor.role}));
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.Message);
            }
        }
    }
}
