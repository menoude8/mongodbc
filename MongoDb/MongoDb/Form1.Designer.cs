﻿namespace MongoDb
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ls_film = new System.Windows.Forms.ListView();
            this.Title = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Genre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbl_title = new System.Windows.Forms.Label();
            this.txt_title = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ls_actor = new System.Windows.Forms.ListView();
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.role = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // ls_film
            // 
            this.ls_film.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Title,
            this.Genre});
            this.ls_film.FullRowSelect = true;
            this.ls_film.Location = new System.Drawing.Point(462, 62);
            this.ls_film.Name = "ls_film";
            this.ls_film.Size = new System.Drawing.Size(164, 418);
            this.ls_film.TabIndex = 0;
            this.ls_film.UseCompatibleStateImageBehavior = false;
            this.ls_film.View = System.Windows.Forms.View.Details;
            this.ls_film.SelectedIndexChanged += new System.EventHandler(this.ls_film_SelectedIndexChanged);
            // 
            // Title
            // 
            this.Title.Text = "Title";
            this.Title.Width = 100;
            // 
            // Genre
            // 
            this.Genre.Text = "Genre";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Location = new System.Drawing.Point(12, 42);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(33, 13);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "Title :";
            // 
            // txt_title
            // 
            this.txt_title.Location = new System.Drawing.Point(51, 39);
            this.txt_title.Name = "txt_title";
            this.txt_title.Size = new System.Drawing.Size(100, 20);
            this.txt_title.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Actors :";
            // 
            // ls_actor
            // 
            this.ls_actor.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.role});
            this.ls_actor.FullRowSelect = true;
            this.ls_actor.Location = new System.Drawing.Point(61, 110);
            this.ls_actor.Name = "ls_actor";
            this.ls_actor.Size = new System.Drawing.Size(164, 159);
            this.ls_actor.TabIndex = 4;
            this.ls_actor.UseCompatibleStateImageBehavior = false;
            this.ls_actor.View = System.Windows.Forms.View.Details;
            // 
            // name
            // 
            this.name.Text = "Name";
            this.name.Width = 100;
            // 
            // role
            // 
            this.role.Text = "Role";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 502);
            this.Controls.Add(this.ls_actor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_title);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.ls_film);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ls_film;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.TextBox txt_title;
        private System.Windows.Forms.ColumnHeader Title;
        private System.Windows.Forms.ColumnHeader Genre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView ls_actor;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader role;
    }
}

