﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace MongoDb
{
    class Film
    {
        public ObjectId Id { get; set; }
        public string Title = "";
        public List<string> Genre = new List<string>();
		public List<Actor> Actor = new List<Actor>();
		public List<Director> Director = new List<Director>();
		public DateTime DateCreation = new DateTime();
		public DateTime DateSortieDvd = new DateTime();
		public List<Trailer> Trailer = new List<Trailer>();
		public int Budget;
		public List<string> Country = new List<string>();
		public List<string> Commentaire =new List<string>();
		public int NbEntry;
		public List<string> Poster = new List<Poster>();
		public int rate;
		public int nbRater;
    }
	
	public class Actor
	{
		public ObjectId Id { get; set; }
		public string name;
		public string role;
			
	}
	
	public class Director
	{
		public ObjectId Id { get; set; }
		public string name;
	}
	
	public class Trailer
	{
		public string url;
		public string name;
		public string description;
	}
	
	public class Commentaire
	{
		public string name;
		public string message;
		public int rate;
	}
}
